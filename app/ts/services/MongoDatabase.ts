
import { MongoClient, MongoError, Db } from 'mongodb';
import { CONFIG } from '../CONFIG';

export interface MongoDataBase {
    connect(): Promise<Db>;
    close(): Promise<void>;
}

class PrivateMongoDataBase implements MongoDataBase {

    readonly url = CONFIG.url;
    readonly dbName = CONFIG.dbName;

    private client: MongoClient;
    private dB: Db;

    connect(): Promise<Db> {
        
        if ( this.dB ) return Promise.resolve( this.dB );

        return new Promise( ( resolve, reject ) => {
            
            MongoClient.connect( this.url, { useNewUrlParser: true }, (err, client) => {
        
                if( err !== null ) reject( err );
                
                this.client = client;
                this.dB = client.db( this.dbName );
                resolve( this.dB );
            });
        
        });
    }

    close(): Promise<void> {

        if( this.client )
            return this.client.close();
        else throw new MongoError('No activated client. This can be due to asynchronous database laoding.');

    }

}

export const mongoDatabase: MongoDataBase = new PrivateMongoDataBase;