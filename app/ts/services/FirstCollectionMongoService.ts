import { mongoDatabase } from "./MongoDatabase";
import { MongoService } from "./MongoService";
import { FirstCollection, FirstCollectionLiteral, FirstCollectionFilter, FirstCollectionName } from "../classes/FirstCollection";

export const firstCollectionMongoService: MongoService<FirstCollection, FirstCollectionLiteral, FirstCollectionFilter> = new MongoService( mongoDatabase, FirstCollection as new () => FirstCollection, FirstCollectionName );