import { MongoDataBase } from "./MongoDatabase";
import { Collection, CollectionLiteral, CollectionFilter } from "../classes/Collection";
import { isArray } from "util";
import { DeleteWriteOpResultObject, UpdateWriteOpResult, InsertOneWriteOpResult, InsertWriteOpResult } from "mongodb";

export class MongoService<T extends Collection, K extends CollectionLiteral, U extends CollectionFilter> {

    protected pDb: MongoDataBase;
    private collection: string;
    private modelClass:{new ():T};

    constructor( pDb: MongoDataBase, modelClass:{new ():T}, collection: string ) {
        this.pDb = pDb;
        this.collection = collection;
        this.modelClass = modelClass;
    }

    resolveModel( data: T|K ): K {

        if( data instanceof this.modelClass ) {
            return (data as T).toJSON() as K;
        }
        else return data as K;

    }

    resolveModels( datas: (T|K)[] ): K[] {


        if( !isArray( datas ) ) return datas as K[];

        const ldatas:K[] = [];

        for( let key in datas ) {
            const kdata = datas[key];
            if( kdata instanceof this.modelClass ) {
                ldatas[key] = (kdata as T).toJSON() as K;
            }
            else ldatas[key] = kdata as K;
        }

        return ldatas;

    }

    deleteOne( data: T|K ): Promise<DeleteWriteOpResultObject> {

        const ldata: K = this.resolveModel(data);

        return new Promise( async (resolve, reject) => {

            try{
                const db = await this.pDb.connect();
                const result = await db.collection( this.collection ).deleteOne( ldata );
                resolve( result );
            } catch( error ) {
                reject( error );
            }
        });

    }

    findOne( data: U ): Promise<K | null> {

        return new Promise( async (resolve, reject) => {

            try {
                const db = await this.pDb.connect();
                const result = await db.collection( this.collection ).findOne<K>( data );
                resolve( result);
            } catch( error ) {
                reject( error )
            }

        });

    }

    findMany( data: U ): Promise<K[]> {

        return new Promise( async (resolve, reject) => {

            try {
                const db = await this.pDb.connect();
                const result: K[] = await db.collection( this.collection ).find<K>( data ).toArray();
                resolve(result);
            } catch( error ) {
                reject( error );
            }
                        
        });

    }

    updateOne( data: U, data_upd: T|K ): Promise<UpdateWriteOpResult> {

        const ldata_upd: K = this.resolveModel( data_upd );

        return new Promise( async (resolve, reject) => {

            try {
                const db = await this.pDb.connect();
                const result = await db.collection( this.collection ).updateOne( data, { $set: ldata_upd } );
                resolve( result );
            } catch( error ) {
                reject( error );
            }

        });

    }

    updateMany( data: U, data_upd: T|K ): Promise<UpdateWriteOpResult> {

        const ldata_upd: K = this.resolveModel( data_upd );

        return new Promise( async (resolve, reject) => {

            try {
                const db = await this.pDb.connect();
                const result = await db.collection( this.collection ).updateMany( data, { $set: ldata_upd } );
                resolve( result );
            } catch( error ) {
                reject( error );
            }

        });

    }

    insertOne( data: T|K ): Promise<InsertOneWriteOpResult> {

        const ldata: K = this.resolveModel( data );

        return new Promise( async (resolve, reject) => {

            try {
                const db = await this.pDb.connect();
                const result = await db.collection( this.collection ).insertOne( ldata );
                resolve( result );
            } catch( error ) {
                reject( error );
            }

        });

    }

    insertMany( datas: (T|K)[] ): Promise<InsertWriteOpResult> {

        const ldatas: K[] = this.resolveModels( datas );

        return new Promise( async (resolve, reject) => {

            try {
                const db = await this.pDb.connect();
                const result = await db.collection( this.collection ).insertMany( ldatas );
                resolve( result );
            } catch( error ) {
                reject( error );
            }

        });
    }
}