import { BaseCollection } from "./CollectionNS";
export namespace FirstCollection {

    export class Collection extends BaseCollection.Collection {
    
        private a: string | number;
        private b: string | number;
    
        constructor( a: string | number, b: string | number ) {
            super();
            this.a = a;
            this.b = b;
        }
    
        toJSON(): CollectionLiteral {
            return {
                a: this.a,
                b: this.b
            }
        }
    
    }
    
    export class CollectionLiteral extends BaseCollection.CollectionLiteral {
    
        a: string | number;
        b: string | number;
    
    }
    
    export class CollectionFilter extends BaseCollection.CollectionFilter {
    
        a?: string | number;
        b?: string | number;
    
    }
    
    export const CollectionName: string = 'firstcollection';

}