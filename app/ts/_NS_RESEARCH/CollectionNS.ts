export namespace BaseCollection {
    
    export abstract class Collection {
        
        toJSON(): CollectionLiteral {
            return {}
        }
    
    }
    export abstract class CollectionLiteral {}
    
    export abstract class CollectionFilter {}
    
    export const CollectionName: string = '';

}