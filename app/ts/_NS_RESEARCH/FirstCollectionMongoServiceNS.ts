import { mongoDatabase } from "../services/MongoDatabase";
import { MongoService } from "../services/MongoService";
import { FirstCollection as FS } from "./FirstCollectionNS";

export const firstCollectionMongoService: MongoService<FS.Collection, FS.CollectionLiteral, FS.CollectionFilter> = new MongoService( mongoDatabase, FS.Collection as new () => FS.Collection, FS.CollectionName );