import { Collection, CollectionLiteral, CollectionFilter } from "./Collection";

export class FirstCollection extends Collection {

    private a: string | number;
    private b: string | number;

    constructor( a: string | number, b: string | number ) {
        super();
        this.a = a;
        this.b = b;
    }

    toJSON(): FirstCollectionLiteral {
        return {
            a: this.a,
            b: this.b
        }
    }

}

export interface FirstCollectionLiteral extends CollectionLiteral {

    a: string | number;
    b: string | number;

}

export class FirstCollectionFilter extends CollectionFilter {

    a?: string | number;
    b?: string | number;

}

export const FirstCollectionName: string = 'firstcollection';