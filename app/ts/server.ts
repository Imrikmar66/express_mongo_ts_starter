import * as express from "express";
import "../scss/styles";
import { firstCollectionMongoService } from "./services/FirstCollectionMongoService";
import { FirstCollection } from "./classes/FirstCollection";
const port: number = 9001;

const app: express.Application = express();

app.set('view engine', 'ejs');
app.use(express.static('public')),

app.post( '/store', ( req: express.Request, res: express.Response ) => {

    firstCollectionMongoService
        .insertMany([ { a: 12, b: 13}, new FirstCollection(45, 78) ])
        .then( console.log )
        .catch( console.log );

    res.render('home');

});

app.get( '/', async ( req: express.Request, res: express.Response ) => {

    firstCollectionMongoService
        .findMany({ b: 13 })
        .then( console.log )
        .catch( console.log );

    res.render('home');

});

app.listen( port, () => { console.log( `Listening on port ${port}` ); });
