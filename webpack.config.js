const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    mode: 'development',
    entry: './app/ts/server.ts',
    target: 'node',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    externals: [nodeExternals()],
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['.ts', '.js', '.scss', '.ejs']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            { 
                test: /\.ejs$/, 
                use: 'ejs-loader' 
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    "css-loader",
                    "sass-loader"
                ]
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin(
            [
                { from:'app/views', to: 'views' },
                { from:'app/public', to: 'public' }
            ],
            { debug: 'info' }
        ),
        new MiniCssExtractPlugin({
            filename: "public/css/styles.css"
          })
    ]
};