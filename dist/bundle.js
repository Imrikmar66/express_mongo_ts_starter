/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/ts/server.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/scss/styles.scss":
/*!******************************!*\
  !*** ./app/scss/styles.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./app/ts/CONFIG.ts":
/*!**************************!*\
  !*** ./app/ts/CONFIG.ts ***!
  \**************************/
/*! exports provided: CONFIG */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CONFIG", function() { return CONFIG; });
const CONFIG = {
    url: 'mongodb://localhost:27017',
    dbName: 'test'
};


/***/ }),

/***/ "./app/ts/classes/Collection.ts":
/*!**************************************!*\
  !*** ./app/ts/classes/Collection.ts ***!
  \**************************************/
/*! exports provided: Collection, CollectionLiteral, CollectionFilter, CollectionName */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Collection", function() { return Collection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionLiteral", function() { return CollectionLiteral; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionFilter", function() { return CollectionFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionName", function() { return CollectionName; });
class Collection {
    toJSON() {
        return {};
    }
}
class CollectionLiteral {
}
class CollectionFilter {
}
const CollectionName = '';


/***/ }),

/***/ "./app/ts/classes/FirstCollection.ts":
/*!*******************************************!*\
  !*** ./app/ts/classes/FirstCollection.ts ***!
  \*******************************************/
/*! exports provided: FirstCollection, FirstCollectionFilter, FirstCollectionName */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirstCollection", function() { return FirstCollection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirstCollectionFilter", function() { return FirstCollectionFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirstCollectionName", function() { return FirstCollectionName; });
/* harmony import */ var _Collection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Collection */ "./app/ts/classes/Collection.ts");

class FirstCollection extends _Collection__WEBPACK_IMPORTED_MODULE_0__["Collection"] {
    constructor(a, b) {
        super();
        this.a = a;
        this.b = b;
    }
    toJSON() {
        return {
            a: this.a,
            b: this.b
        };
    }
}
class FirstCollectionFilter extends _Collection__WEBPACK_IMPORTED_MODULE_0__["CollectionFilter"] {
}
const FirstCollectionName = 'firstcollection';


/***/ }),

/***/ "./app/ts/server.ts":
/*!**************************!*\
  !*** ./app/ts/server.ts ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _scss_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../scss/styles */ "./app/scss/styles.scss");
/* harmony import */ var _scss_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_scss_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _services_FirstCollectionMongoService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/FirstCollectionMongoService */ "./app/ts/services/FirstCollectionMongoService.ts");
/* harmony import */ var _classes_FirstCollection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./classes/FirstCollection */ "./app/ts/classes/FirstCollection.ts");
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};




const port = 9001;
const app = express__WEBPACK_IMPORTED_MODULE_0__();
app.set('view engine', 'ejs');
app.use(express__WEBPACK_IMPORTED_MODULE_0__["static"]('public')),
    app.post('/store', (req, res) => {
        _services_FirstCollectionMongoService__WEBPACK_IMPORTED_MODULE_2__["firstCollectionMongoService"]
            .insertMany([{ a: 12, b: 13 }, new _classes_FirstCollection__WEBPACK_IMPORTED_MODULE_3__["FirstCollection"](45, 78)])
            .then(console.log)
            .catch(console.log);
        res.render('home');
    });
app.get('/', (req, res) => __awaiter(undefined, void 0, void 0, function* () {
    _services_FirstCollectionMongoService__WEBPACK_IMPORTED_MODULE_2__["firstCollectionMongoService"]
        .findMany({ b: 13 })
        .then(console.log)
        .catch(console.log);
    res.render('home');
}));
app.listen(port, () => { console.log(`Listening on port ${port}`); });


/***/ }),

/***/ "./app/ts/services/FirstCollectionMongoService.ts":
/*!********************************************************!*\
  !*** ./app/ts/services/FirstCollectionMongoService.ts ***!
  \********************************************************/
/*! exports provided: firstCollectionMongoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "firstCollectionMongoService", function() { return firstCollectionMongoService; });
/* harmony import */ var _MongoDatabase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MongoDatabase */ "./app/ts/services/MongoDatabase.ts");
/* harmony import */ var _MongoService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MongoService */ "./app/ts/services/MongoService.ts");
/* harmony import */ var _classes_FirstCollection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classes/FirstCollection */ "./app/ts/classes/FirstCollection.ts");



const firstCollectionMongoService = new _MongoService__WEBPACK_IMPORTED_MODULE_1__["MongoService"](_MongoDatabase__WEBPACK_IMPORTED_MODULE_0__["mongoDatabase"], _classes_FirstCollection__WEBPACK_IMPORTED_MODULE_2__["FirstCollection"], _classes_FirstCollection__WEBPACK_IMPORTED_MODULE_2__["FirstCollectionName"]);


/***/ }),

/***/ "./app/ts/services/MongoDatabase.ts":
/*!******************************************!*\
  !*** ./app/ts/services/MongoDatabase.ts ***!
  \******************************************/
/*! exports provided: mongoDatabase */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mongoDatabase", function() { return mongoDatabase; });
/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongodb */ "mongodb");
/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongodb__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CONFIG__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../CONFIG */ "./app/ts/CONFIG.ts");


class PrivateMongoDataBase {
    constructor() {
        this.url = _CONFIG__WEBPACK_IMPORTED_MODULE_1__["CONFIG"].url;
        this.dbName = _CONFIG__WEBPACK_IMPORTED_MODULE_1__["CONFIG"].dbName;
    }
    connect() {
        if (this.dB)
            return Promise.resolve(this.dB);
        return new Promise((resolve, reject) => {
            mongodb__WEBPACK_IMPORTED_MODULE_0__["MongoClient"].connect(this.url, { useNewUrlParser: true }, (err, client) => {
                if (err !== null)
                    reject(err);
                this.client = client;
                this.dB = client.db(this.dbName);
                resolve(this.dB);
            });
        });
    }
    close() {
        if (this.client)
            return this.client.close();
        else
            throw new mongodb__WEBPACK_IMPORTED_MODULE_0__["MongoError"]('No activated client. This can be due to asynchronous database laoding.');
    }
}
const mongoDatabase = new PrivateMongoDataBase;


/***/ }),

/***/ "./app/ts/services/MongoService.ts":
/*!*****************************************!*\
  !*** ./app/ts/services/MongoService.ts ***!
  \*****************************************/
/*! exports provided: MongoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MongoService", function() { return MongoService; });
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! util */ "util");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_0__);
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};

class MongoService {
    constructor(pDb, modelClass, collection) {
        this.pDb = pDb;
        this.collection = collection;
        this.modelClass = modelClass;
    }
    resolveModel(data) {
        if (data instanceof this.modelClass) {
            return data.toJSON();
        }
        else
            return data;
    }
    resolveModels(datas) {
        if (!Object(util__WEBPACK_IMPORTED_MODULE_0__["isArray"])(datas))
            return datas;
        const ldatas = [];
        for (let key in datas) {
            const kdata = datas[key];
            if (kdata instanceof this.modelClass) {
                ldatas[key] = kdata.toJSON();
            }
            else
                ldatas[key] = kdata;
        }
        return ldatas;
    }
    deleteOne(data) {
        const ldata = this.resolveModel(data);
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield this.pDb.connect();
                const result = yield db.collection(this.collection).deleteOne(ldata);
                resolve(result);
            }
            catch (error) {
                reject(error);
            }
        }));
    }
    findOne(data) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield this.pDb.connect();
                const result = yield db.collection(this.collection).findOne(data);
                resolve(result);
            }
            catch (error) {
                reject(error);
            }
        }));
    }
    findMany(data) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield this.pDb.connect();
                const result = yield db.collection(this.collection).find(data).toArray();
                resolve(result);
            }
            catch (error) {
                reject(error);
            }
        }));
    }
    updateOne(data, data_upd) {
        const ldata_upd = this.resolveModel(data_upd);
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield this.pDb.connect();
                const result = yield db.collection(this.collection).updateOne(data, { $set: ldata_upd });
                resolve(result);
            }
            catch (error) {
                reject(error);
            }
        }));
    }
    updateMany(data, data_upd) {
        const ldata_upd = this.resolveModel(data_upd);
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield this.pDb.connect();
                const result = yield db.collection(this.collection).updateMany(data, { $set: ldata_upd });
                resolve(result);
            }
            catch (error) {
                reject(error);
            }
        }));
    }
    insertOne(data) {
        const ldata = this.resolveModel(data);
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield this.pDb.connect();
                const result = yield db.collection(this.collection).insertOne(ldata);
                resolve(result);
            }
            catch (error) {
                reject(error);
            }
        }));
    }
    insertMany(datas) {
        const ldatas = this.resolveModels(datas);
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield this.pDb.connect();
                const result = yield db.collection(this.collection).insertMany(ldatas);
                resolve(result);
            }
            catch (error) {
                reject(error);
            }
        }));
    }
}


/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "mongodb":
/*!**************************!*\
  !*** external "mongodb" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongodb");

/***/ }),

/***/ "util":
/*!***********************!*\
  !*** external "util" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("util");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYXBwL3Njc3Mvc3R5bGVzLnNjc3MiLCJ3ZWJwYWNrOi8vLy4vYXBwL3RzL0NPTkZJRy50cyIsIndlYnBhY2s6Ly8vLi9hcHAvdHMvY2xhc3Nlcy9Db2xsZWN0aW9uLnRzIiwid2VicGFjazovLy8uL2FwcC90cy9jbGFzc2VzL0ZpcnN0Q29sbGVjdGlvbi50cyIsIndlYnBhY2s6Ly8vLi9hcHAvdHMvc2VydmVyLnRzIiwid2VicGFjazovLy8uL2FwcC90cy9zZXJ2aWNlcy9GaXJzdENvbGxlY3Rpb25Nb25nb1NlcnZpY2UudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3RzL3NlcnZpY2VzL01vbmdvRGF0YWJhc2UudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3RzL3NlcnZpY2VzL01vbmdvU2VydmljZS50cyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJleHByZXNzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibW9uZ29kYlwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInV0aWxcIiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNsRkEsdUM7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBTyxNQUFNLE1BQU0sR0FBcUM7SUFDcEQsR0FBRyxFQUFFLDJCQUEyQjtJQUNoQyxNQUFNLEVBQUUsTUFBTTtDQUNqQjs7Ozs7Ozs7Ozs7OztBQ0hEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBTyxNQUFlLFVBQVU7SUFFNUIsTUFBTTtRQUNGLE9BQU8sRUFBRTtJQUNiLENBQUM7Q0FFSjtBQUNNLE1BQWUsaUJBQWlCO0NBQUc7QUFFbkMsTUFBZSxnQkFBZ0I7Q0FBRztBQUVsQyxNQUFNLGNBQWMsR0FBVyxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7QUNYekM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUErRTtBQUV4RSxNQUFNLGVBQWdCLFNBQVEsc0RBQVU7SUFLM0MsWUFBYSxDQUFrQixFQUFFLENBQWtCO1FBQy9DLEtBQUssRUFBRSxDQUFDO1FBQ1IsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDWCxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNmLENBQUM7SUFFRCxNQUFNO1FBQ0YsT0FBTztZQUNILENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNULENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUNaO0lBQ0wsQ0FBQztDQUVKO0FBU00sTUFBTSxxQkFBc0IsU0FBUSw0REFBZ0I7Q0FLMUQ7QUFFTSxNQUFNLG1CQUFtQixHQUFXLGlCQUFpQixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcEMxQjtBQUNYO0FBQzZEO0FBQ3pCO0FBQzVELE1BQU0sSUFBSSxHQUFXLElBQUksQ0FBQztBQUUxQixNQUFNLEdBQUcsR0FBd0Isb0NBQU8sRUFBRSxDQUFDO0FBRTNDLEdBQUcsQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQzlCLEdBQUcsQ0FBQyxHQUFHLENBQUMsOENBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUVqQyxHQUFHLENBQUMsSUFBSSxDQUFFLFFBQVEsRUFBRSxDQUFFLEdBQW9CLEVBQUUsR0FBcUIsRUFBRyxFQUFFO1FBRWxFLGlHQUEyQjthQUN0QixVQUFVLENBQUMsQ0FBRSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBQyxFQUFFLElBQUksd0VBQWUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUUsQ0FBQzthQUM1RCxJQUFJLENBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBRTthQUNuQixLQUFLLENBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBRSxDQUFDO1FBRTFCLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7SUFFdkIsQ0FBQyxDQUFDLENBQUM7QUFFSCxHQUFHLENBQUMsR0FBRyxDQUFFLEdBQUcsRUFBRSxDQUFRLEdBQW9CLEVBQUUsR0FBcUIsRUFBRyxFQUFFO0lBRWxFLGlHQUEyQjtTQUN0QixRQUFRLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUM7U0FDbkIsSUFBSSxDQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUU7U0FDbkIsS0FBSyxDQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUUsQ0FBQztJQUUxQixHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBRXZCLENBQUMsRUFBQyxDQUFDO0FBRUgsR0FBRyxDQUFDLE1BQU0sQ0FBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBRSxxQkFBcUIsSUFBSSxFQUFFLENBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7O0FDakN6RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWdEO0FBQ0Y7QUFDbUY7QUFFMUgsTUFBTSwyQkFBMkIsR0FBaUYsSUFBSSwwREFBWSxDQUFFLDREQUFhLEVBQUUsd0VBQTRDLEVBQUUsNEVBQW1CLENBQUUsQ0FBQzs7Ozs7Ozs7Ozs7OztBQ0g5TjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXNEO0FBQ25CO0FBT25DLE1BQU0sb0JBQW9CO0lBQTFCO1FBRWEsUUFBRyxHQUFHLDhDQUFNLENBQUMsR0FBRyxDQUFDO1FBQ2pCLFdBQU0sR0FBRyw4Q0FBTSxDQUFDLE1BQU0sQ0FBQztJQStCcEMsQ0FBQztJQTFCRyxPQUFPO1FBRUgsSUFBSyxJQUFJLENBQUMsRUFBRTtZQUFHLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBRSxJQUFJLENBQUMsRUFBRSxDQUFFLENBQUM7UUFFakQsT0FBTyxJQUFJLE9BQU8sQ0FBRSxDQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUcsRUFBRTtZQUV0QyxtREFBVyxDQUFDLE9BQU8sQ0FBRSxJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFO2dCQUV0RSxJQUFJLEdBQUcsS0FBSyxJQUFJO29CQUFHLE1BQU0sQ0FBRSxHQUFHLENBQUUsQ0FBQztnQkFFakMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBRSxJQUFJLENBQUMsTUFBTSxDQUFFLENBQUM7Z0JBQ25DLE9BQU8sQ0FBRSxJQUFJLENBQUMsRUFBRSxDQUFFLENBQUM7WUFDdkIsQ0FBQyxDQUFDLENBQUM7UUFFUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxLQUFLO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTTtZQUNYLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7WUFDMUIsTUFBTSxJQUFJLGtEQUFVLENBQUMsd0VBQXdFLENBQUMsQ0FBQztJQUV4RyxDQUFDO0NBRUo7QUFFTSxNQUFNLGFBQWEsR0FBa0IsSUFBSSxvQkFBb0IsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNDdEM7QUFHeEIsTUFBTSxZQUFZO0lBTXJCLFlBQWEsR0FBa0IsRUFBRSxVQUFxQixFQUFFLFVBQWtCO1FBQ3RFLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDN0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7SUFDakMsQ0FBQztJQUVELFlBQVksQ0FBRSxJQUFTO1FBRW5CLElBQUksSUFBSSxZQUFZLElBQUksQ0FBQyxVQUFVLEVBQUc7WUFDbEMsT0FBUSxJQUFVLENBQUMsTUFBTSxFQUFPLENBQUM7U0FDcEM7O1lBQ0ksT0FBTyxJQUFTLENBQUM7SUFFMUIsQ0FBQztJQUVELGFBQWEsQ0FBRSxLQUFjO1FBR3pCLElBQUksQ0FBQyxvREFBTyxDQUFFLEtBQUssQ0FBRTtZQUFHLE9BQU8sS0FBWSxDQUFDO1FBRTVDLE1BQU0sTUFBTSxHQUFPLEVBQUUsQ0FBQztRQUV0QixLQUFLLElBQUksR0FBRyxJQUFJLEtBQUssRUFBRztZQUNwQixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDekIsSUFBSSxLQUFLLFlBQVksSUFBSSxDQUFDLFVBQVUsRUFBRztnQkFDbkMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFJLEtBQVcsQ0FBQyxNQUFNLEVBQU8sQ0FBQzthQUM1Qzs7Z0JBQ0ksTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQVUsQ0FBQztTQUNqQztRQUVELE9BQU8sTUFBTSxDQUFDO0lBRWxCLENBQUM7SUFFRCxTQUFTLENBQUUsSUFBUztRQUVoQixNQUFNLEtBQUssR0FBTSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXpDLE9BQU8sSUFBSSxPQUFPLENBQUUsQ0FBTyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFMUMsSUFBRztnQkFDQyxNQUFNLEVBQUUsR0FBRyxNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3BDLE1BQU0sTUFBTSxHQUFHLE1BQU0sRUFBRSxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFFLENBQUMsU0FBUyxDQUFFLEtBQUssQ0FBRSxDQUFDO2dCQUN6RSxPQUFPLENBQUUsTUFBTSxDQUFFLENBQUM7YUFDckI7WUFBQyxPQUFPLEtBQUssRUFBRztnQkFDYixNQUFNLENBQUUsS0FBSyxDQUFFLENBQUM7YUFDbkI7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCxPQUFPLENBQUUsSUFBTztRQUVaLE9BQU8sSUFBSSxPQUFPLENBQUUsQ0FBTyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFMUMsSUFBSTtnQkFDQSxNQUFNLEVBQUUsR0FBRyxNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3BDLE1BQU0sTUFBTSxHQUFHLE1BQU0sRUFBRSxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFFLENBQUMsT0FBTyxDQUFLLElBQUksQ0FBRSxDQUFDO2dCQUN6RSxPQUFPLENBQUUsTUFBTSxDQUFDLENBQUM7YUFDcEI7WUFBQyxPQUFPLEtBQUssRUFBRztnQkFDYixNQUFNLENBQUUsS0FBSyxDQUFFO2FBQ2xCO1FBRUwsQ0FBQyxFQUFDLENBQUM7SUFFUCxDQUFDO0lBRUQsUUFBUSxDQUFFLElBQU87UUFFYixPQUFPLElBQUksT0FBTyxDQUFFLENBQU8sT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBRTFDLElBQUk7Z0JBQ0EsTUFBTSxFQUFFLEdBQUcsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNwQyxNQUFNLE1BQU0sR0FBUSxNQUFNLEVBQUUsQ0FBQyxVQUFVLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBRSxDQUFDLElBQUksQ0FBSyxJQUFJLENBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDckYsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ25CO1lBQUMsT0FBTyxLQUFLLEVBQUc7Z0JBQ2IsTUFBTSxDQUFFLEtBQUssQ0FBRSxDQUFDO2FBQ25CO1FBRUwsQ0FBQyxFQUFDLENBQUM7SUFFUCxDQUFDO0lBRUQsU0FBUyxDQUFFLElBQU8sRUFBRSxRQUFhO1FBRTdCLE1BQU0sU0FBUyxHQUFNLElBQUksQ0FBQyxZQUFZLENBQUUsUUFBUSxDQUFFLENBQUM7UUFFbkQsT0FBTyxJQUFJLE9BQU8sQ0FBRSxDQUFPLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUUxQyxJQUFJO2dCQUNBLE1BQU0sRUFBRSxHQUFHLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDcEMsTUFBTSxNQUFNLEdBQUcsTUFBTSxFQUFFLENBQUMsVUFBVSxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUUsQ0FBQyxTQUFTLENBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFFLENBQUM7Z0JBQzdGLE9BQU8sQ0FBRSxNQUFNLENBQUUsQ0FBQzthQUNyQjtZQUFDLE9BQU8sS0FBSyxFQUFHO2dCQUNiLE1BQU0sQ0FBRSxLQUFLLENBQUUsQ0FBQzthQUNuQjtRQUVMLENBQUMsRUFBQyxDQUFDO0lBRVAsQ0FBQztJQUVELFVBQVUsQ0FBRSxJQUFPLEVBQUUsUUFBYTtRQUU5QixNQUFNLFNBQVMsR0FBTSxJQUFJLENBQUMsWUFBWSxDQUFFLFFBQVEsQ0FBRSxDQUFDO1FBRW5ELE9BQU8sSUFBSSxPQUFPLENBQUUsQ0FBTyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFMUMsSUFBSTtnQkFDQSxNQUFNLEVBQUUsR0FBRyxNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3BDLE1BQU0sTUFBTSxHQUFHLE1BQU0sRUFBRSxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFFLENBQUMsVUFBVSxDQUFFLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsQ0FBRSxDQUFDO2dCQUM5RixPQUFPLENBQUUsTUFBTSxDQUFFLENBQUM7YUFDckI7WUFBQyxPQUFPLEtBQUssRUFBRztnQkFDYixNQUFNLENBQUUsS0FBSyxDQUFFLENBQUM7YUFDbkI7UUFFTCxDQUFDLEVBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCxTQUFTLENBQUUsSUFBUztRQUVoQixNQUFNLEtBQUssR0FBTSxJQUFJLENBQUMsWUFBWSxDQUFFLElBQUksQ0FBRSxDQUFDO1FBRTNDLE9BQU8sSUFBSSxPQUFPLENBQUUsQ0FBTyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFMUMsSUFBSTtnQkFDQSxNQUFNLEVBQUUsR0FBRyxNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3BDLE1BQU0sTUFBTSxHQUFHLE1BQU0sRUFBRSxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFFLENBQUMsU0FBUyxDQUFFLEtBQUssQ0FBRSxDQUFDO2dCQUN6RSxPQUFPLENBQUUsTUFBTSxDQUFFLENBQUM7YUFDckI7WUFBQyxPQUFPLEtBQUssRUFBRztnQkFDYixNQUFNLENBQUUsS0FBSyxDQUFFLENBQUM7YUFDbkI7UUFFTCxDQUFDLEVBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCxVQUFVLENBQUUsS0FBYztRQUV0QixNQUFNLE1BQU0sR0FBUSxJQUFJLENBQUMsYUFBYSxDQUFFLEtBQUssQ0FBRSxDQUFDO1FBRWhELE9BQU8sSUFBSSxPQUFPLENBQUUsQ0FBTyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFMUMsSUFBSTtnQkFDQSxNQUFNLEVBQUUsR0FBRyxNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3BDLE1BQU0sTUFBTSxHQUFHLE1BQU0sRUFBRSxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFFLENBQUMsVUFBVSxDQUFFLE1BQU0sQ0FBRSxDQUFDO2dCQUMzRSxPQUFPLENBQUUsTUFBTSxDQUFFLENBQUM7YUFDckI7WUFBQyxPQUFPLEtBQUssRUFBRztnQkFDYixNQUFNLENBQUUsS0FBSyxDQUFFLENBQUM7YUFDbkI7UUFFTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Q0FDSjs7Ozs7Ozs7Ozs7O0FDcEtELG9DOzs7Ozs7Ozs7OztBQ0FBLG9DOzs7Ozs7Ozs7OztBQ0FBLGlDIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vYXBwL3RzL3NlcnZlci50c1wiKTtcbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsImV4cG9ydCBjb25zdCBDT05GSUc6IHsgdXJsOiBzdHJpbmcsICBkYk5hbWU6IHN0cmluZyB9ID0ge1xyXG4gICAgdXJsOiAnbW9uZ29kYjovL2xvY2FsaG9zdDoyNzAxNycsXHJcbiAgICBkYk5hbWU6ICd0ZXN0J1xyXG59IiwiZXhwb3J0IGFic3RyYWN0IGNsYXNzIENvbGxlY3Rpb24ge1xyXG4gICAgXHJcbiAgICB0b0pTT04oKTogQ29sbGVjdGlvbkxpdGVyYWwge1xyXG4gICAgICAgIHJldHVybiB7fVxyXG4gICAgfVxyXG5cclxufVxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQ29sbGVjdGlvbkxpdGVyYWwge31cclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBDb2xsZWN0aW9uRmlsdGVyIHt9XHJcblxyXG5leHBvcnQgY29uc3QgQ29sbGVjdGlvbk5hbWU6IHN0cmluZyA9ICcnOyIsImltcG9ydCB7IENvbGxlY3Rpb24sIENvbGxlY3Rpb25MaXRlcmFsLCBDb2xsZWN0aW9uRmlsdGVyIH0gZnJvbSBcIi4vQ29sbGVjdGlvblwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEZpcnN0Q29sbGVjdGlvbiBleHRlbmRzIENvbGxlY3Rpb24ge1xyXG5cclxuICAgIHByaXZhdGUgYTogc3RyaW5nIHwgbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBiOiBzdHJpbmcgfCBudW1iZXI7XHJcblxyXG4gICAgY29uc3RydWN0b3IoIGE6IHN0cmluZyB8IG51bWJlciwgYjogc3RyaW5nIHwgbnVtYmVyICkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5hID0gYTtcclxuICAgICAgICB0aGlzLmIgPSBiO1xyXG4gICAgfVxyXG5cclxuICAgIHRvSlNPTigpOiBGaXJzdENvbGxlY3Rpb25MaXRlcmFsIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBhOiB0aGlzLmEsXHJcbiAgICAgICAgICAgIGI6IHRoaXMuYlxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmlyc3RDb2xsZWN0aW9uTGl0ZXJhbCBleHRlbmRzIENvbGxlY3Rpb25MaXRlcmFsIHtcclxuXHJcbiAgICBhOiBzdHJpbmcgfCBudW1iZXI7XHJcbiAgICBiOiBzdHJpbmcgfCBudW1iZXI7XHJcblxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRmlyc3RDb2xsZWN0aW9uRmlsdGVyIGV4dGVuZHMgQ29sbGVjdGlvbkZpbHRlciB7XHJcblxyXG4gICAgYT86IHN0cmluZyB8IG51bWJlcjtcclxuICAgIGI/OiBzdHJpbmcgfCBudW1iZXI7XHJcblxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgRmlyc3RDb2xsZWN0aW9uTmFtZTogc3RyaW5nID0gJ2ZpcnN0Y29sbGVjdGlvbic7IiwiaW1wb3J0ICogYXMgZXhwcmVzcyBmcm9tIFwiZXhwcmVzc1wiO1xyXG5pbXBvcnQgXCIuLi9zY3NzL3N0eWxlc1wiO1xyXG5pbXBvcnQgeyBmaXJzdENvbGxlY3Rpb25Nb25nb1NlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9GaXJzdENvbGxlY3Rpb25Nb25nb1NlcnZpY2VcIjtcclxuaW1wb3J0IHsgRmlyc3RDb2xsZWN0aW9uIH0gZnJvbSBcIi4vY2xhc3Nlcy9GaXJzdENvbGxlY3Rpb25cIjtcclxuY29uc3QgcG9ydDogbnVtYmVyID0gOTAwMTtcclxuXHJcbmNvbnN0IGFwcDogZXhwcmVzcy5BcHBsaWNhdGlvbiA9IGV4cHJlc3MoKTtcclxuXHJcbmFwcC5zZXQoJ3ZpZXcgZW5naW5lJywgJ2VqcycpO1xyXG5hcHAudXNlKGV4cHJlc3Muc3RhdGljKCdwdWJsaWMnKSksXHJcblxyXG5hcHAucG9zdCggJy9zdG9yZScsICggcmVxOiBleHByZXNzLlJlcXVlc3QsIHJlczogZXhwcmVzcy5SZXNwb25zZSApID0+IHtcclxuXHJcbiAgICBmaXJzdENvbGxlY3Rpb25Nb25nb1NlcnZpY2VcclxuICAgICAgICAuaW5zZXJ0TWFueShbIHsgYTogMTIsIGI6IDEzfSwgbmV3IEZpcnN0Q29sbGVjdGlvbig0NSwgNzgpIF0pXHJcbiAgICAgICAgLnRoZW4oIGNvbnNvbGUubG9nIClcclxuICAgICAgICAuY2F0Y2goIGNvbnNvbGUubG9nICk7XHJcblxyXG4gICAgcmVzLnJlbmRlcignaG9tZScpO1xyXG5cclxufSk7XHJcblxyXG5hcHAuZ2V0KCAnLycsIGFzeW5jICggcmVxOiBleHByZXNzLlJlcXVlc3QsIHJlczogZXhwcmVzcy5SZXNwb25zZSApID0+IHtcclxuXHJcbiAgICBmaXJzdENvbGxlY3Rpb25Nb25nb1NlcnZpY2VcclxuICAgICAgICAuZmluZE1hbnkoeyBiOiAxMyB9KVxyXG4gICAgICAgIC50aGVuKCBjb25zb2xlLmxvZyApXHJcbiAgICAgICAgLmNhdGNoKCBjb25zb2xlLmxvZyApO1xyXG5cclxuICAgIHJlcy5yZW5kZXIoJ2hvbWUnKTtcclxuXHJcbn0pO1xyXG5cclxuYXBwLmxpc3RlbiggcG9ydCwgKCkgPT4geyBjb25zb2xlLmxvZyggYExpc3RlbmluZyBvbiBwb3J0ICR7cG9ydH1gICk7IH0pO1xyXG4iLCJpbXBvcnQgeyBtb25nb0RhdGFiYXNlIH0gZnJvbSBcIi4vTW9uZ29EYXRhYmFzZVwiO1xyXG5pbXBvcnQgeyBNb25nb1NlcnZpY2UgfSBmcm9tIFwiLi9Nb25nb1NlcnZpY2VcIjtcclxuaW1wb3J0IHsgRmlyc3RDb2xsZWN0aW9uLCBGaXJzdENvbGxlY3Rpb25MaXRlcmFsLCBGaXJzdENvbGxlY3Rpb25GaWx0ZXIsIEZpcnN0Q29sbGVjdGlvbk5hbWUgfSBmcm9tIFwiLi4vY2xhc3Nlcy9GaXJzdENvbGxlY3Rpb25cIjtcclxuXHJcbmV4cG9ydCBjb25zdCBmaXJzdENvbGxlY3Rpb25Nb25nb1NlcnZpY2U6IE1vbmdvU2VydmljZTxGaXJzdENvbGxlY3Rpb24sIEZpcnN0Q29sbGVjdGlvbkxpdGVyYWwsIEZpcnN0Q29sbGVjdGlvbkZpbHRlcj4gPSBuZXcgTW9uZ29TZXJ2aWNlKCBtb25nb0RhdGFiYXNlLCBGaXJzdENvbGxlY3Rpb24gYXMgbmV3ICgpID0+IEZpcnN0Q29sbGVjdGlvbiwgRmlyc3RDb2xsZWN0aW9uTmFtZSApOyIsIlxyXG5pbXBvcnQgeyBNb25nb0NsaWVudCwgTW9uZ29FcnJvciwgRGIgfSBmcm9tICdtb25nb2RiJztcclxuaW1wb3J0IHsgQ09ORklHIH0gZnJvbSAnLi4vQ09ORklHJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgTW9uZ29EYXRhQmFzZSB7XHJcbiAgICBjb25uZWN0KCk6IFByb21pc2U8RGI+O1xyXG4gICAgY2xvc2UoKTogUHJvbWlzZTx2b2lkPjtcclxufVxyXG5cclxuY2xhc3MgUHJpdmF0ZU1vbmdvRGF0YUJhc2UgaW1wbGVtZW50cyBNb25nb0RhdGFCYXNlIHtcclxuXHJcbiAgICByZWFkb25seSB1cmwgPSBDT05GSUcudXJsO1xyXG4gICAgcmVhZG9ubHkgZGJOYW1lID0gQ09ORklHLmRiTmFtZTtcclxuXHJcbiAgICBwcml2YXRlIGNsaWVudDogTW9uZ29DbGllbnQ7XHJcbiAgICBwcml2YXRlIGRCOiBEYjtcclxuXHJcbiAgICBjb25uZWN0KCk6IFByb21pc2U8RGI+IHtcclxuICAgICAgICBcclxuICAgICAgICBpZiAoIHRoaXMuZEIgKSByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCB0aGlzLmRCICk7XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSggKCByZXNvbHZlLCByZWplY3QgKSA9PiB7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBNb25nb0NsaWVudC5jb25uZWN0KCB0aGlzLnVybCwgeyB1c2VOZXdVcmxQYXJzZXI6IHRydWUgfSwgKGVyciwgY2xpZW50KSA9PiB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBpZiggZXJyICE9PSBudWxsICkgcmVqZWN0KCBlcnIgKTtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGllbnQgPSBjbGllbnQ7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRCID0gY2xpZW50LmRiKCB0aGlzLmRiTmFtZSApO1xyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSggdGhpcy5kQiApO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICBcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjbG9zZSgpOiBQcm9taXNlPHZvaWQ+IHtcclxuXHJcbiAgICAgICAgaWYoIHRoaXMuY2xpZW50IClcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY2xpZW50LmNsb3NlKCk7XHJcbiAgICAgICAgZWxzZSB0aHJvdyBuZXcgTW9uZ29FcnJvcignTm8gYWN0aXZhdGVkIGNsaWVudC4gVGhpcyBjYW4gYmUgZHVlIHRvIGFzeW5jaHJvbm91cyBkYXRhYmFzZSBsYW9kaW5nLicpO1xyXG5cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBtb25nb0RhdGFiYXNlOiBNb25nb0RhdGFCYXNlID0gbmV3IFByaXZhdGVNb25nb0RhdGFCYXNlOyIsImltcG9ydCB7IE1vbmdvRGF0YUJhc2UgfSBmcm9tIFwiLi9Nb25nb0RhdGFiYXNlXCI7XHJcbmltcG9ydCB7IENvbGxlY3Rpb24sIENvbGxlY3Rpb25MaXRlcmFsLCBDb2xsZWN0aW9uRmlsdGVyIH0gZnJvbSBcIi4uL2NsYXNzZXMvQ29sbGVjdGlvblwiO1xyXG5pbXBvcnQgeyBpc0FycmF5IH0gZnJvbSBcInV0aWxcIjtcclxuaW1wb3J0IHsgRGVsZXRlV3JpdGVPcFJlc3VsdE9iamVjdCwgVXBkYXRlV3JpdGVPcFJlc3VsdCwgSW5zZXJ0T25lV3JpdGVPcFJlc3VsdCwgSW5zZXJ0V3JpdGVPcFJlc3VsdCB9IGZyb20gXCJtb25nb2RiXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgTW9uZ29TZXJ2aWNlPFQgZXh0ZW5kcyBDb2xsZWN0aW9uLCBLIGV4dGVuZHMgQ29sbGVjdGlvbkxpdGVyYWwsIFUgZXh0ZW5kcyBDb2xsZWN0aW9uRmlsdGVyPiB7XHJcblxyXG4gICAgcHJvdGVjdGVkIHBEYjogTW9uZ29EYXRhQmFzZTtcclxuICAgIHByaXZhdGUgY29sbGVjdGlvbjogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBtb2RlbENsYXNzOntuZXcgKCk6VH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHBEYjogTW9uZ29EYXRhQmFzZSwgbW9kZWxDbGFzczp7bmV3ICgpOlR9LCBjb2xsZWN0aW9uOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgdGhpcy5wRGIgPSBwRGI7XHJcbiAgICAgICAgdGhpcy5jb2xsZWN0aW9uID0gY29sbGVjdGlvbjtcclxuICAgICAgICB0aGlzLm1vZGVsQ2xhc3MgPSBtb2RlbENsYXNzO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc29sdmVNb2RlbCggZGF0YTogVHxLICk6IEsge1xyXG5cclxuICAgICAgICBpZiggZGF0YSBpbnN0YW5jZW9mIHRoaXMubW9kZWxDbGFzcyApIHtcclxuICAgICAgICAgICAgcmV0dXJuIChkYXRhIGFzIFQpLnRvSlNPTigpIGFzIEs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgcmV0dXJuIGRhdGEgYXMgSztcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcmVzb2x2ZU1vZGVscyggZGF0YXM6IChUfEspW10gKTogS1tdIHtcclxuXHJcblxyXG4gICAgICAgIGlmKCAhaXNBcnJheSggZGF0YXMgKSApIHJldHVybiBkYXRhcyBhcyBLW107XHJcblxyXG4gICAgICAgIGNvbnN0IGxkYXRhczpLW10gPSBbXTtcclxuXHJcbiAgICAgICAgZm9yKCBsZXQga2V5IGluIGRhdGFzICkge1xyXG4gICAgICAgICAgICBjb25zdCBrZGF0YSA9IGRhdGFzW2tleV07XHJcbiAgICAgICAgICAgIGlmKCBrZGF0YSBpbnN0YW5jZW9mIHRoaXMubW9kZWxDbGFzcyApIHtcclxuICAgICAgICAgICAgICAgIGxkYXRhc1trZXldID0gKGtkYXRhIGFzIFQpLnRvSlNPTigpIGFzIEs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBsZGF0YXNba2V5XSA9IGtkYXRhIGFzIEs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbGRhdGFzO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVPbmUoIGRhdGE6IFR8SyApOiBQcm9taXNlPERlbGV0ZVdyaXRlT3BSZXN1bHRPYmplY3Q+IHtcclxuXHJcbiAgICAgICAgY29uc3QgbGRhdGE6IEsgPSB0aGlzLnJlc29sdmVNb2RlbChkYXRhKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKCBhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICB0cnl7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYiA9IGF3YWl0IHRoaXMucERiLmNvbm5lY3QoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IGRiLmNvbGxlY3Rpb24oIHRoaXMuY29sbGVjdGlvbiApLmRlbGV0ZU9uZSggbGRhdGEgKTtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoIHJlc3VsdCApO1xyXG4gICAgICAgICAgICB9IGNhdGNoKCBlcnJvciApIHtcclxuICAgICAgICAgICAgICAgIHJlamVjdCggZXJyb3IgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBmaW5kT25lKCBkYXRhOiBVICk6IFByb21pc2U8SyB8IG51bGw+IHtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKCBhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGIgPSBhd2FpdCB0aGlzLnBEYi5jb25uZWN0KCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBkYi5jb2xsZWN0aW9uKCB0aGlzLmNvbGxlY3Rpb24gKS5maW5kT25lPEs+KCBkYXRhICk7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKCByZXN1bHQpO1xyXG4gICAgICAgICAgICB9IGNhdGNoKCBlcnJvciApIHtcclxuICAgICAgICAgICAgICAgIHJlamVjdCggZXJyb3IgKVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBmaW5kTWFueSggZGF0YTogVSApOiBQcm9taXNlPEtbXT4ge1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoIGFzeW5jIChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYiA9IGF3YWl0IHRoaXMucERiLmNvbm5lY3QoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlc3VsdDogS1tdID0gYXdhaXQgZGIuY29sbGVjdGlvbiggdGhpcy5jb2xsZWN0aW9uICkuZmluZDxLPiggZGF0YSApLnRvQXJyYXkoKTtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcclxuICAgICAgICAgICAgfSBjYXRjaCggZXJyb3IgKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QoIGVycm9yICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZU9uZSggZGF0YTogVSwgZGF0YV91cGQ6IFR8SyApOiBQcm9taXNlPFVwZGF0ZVdyaXRlT3BSZXN1bHQ+IHtcclxuXHJcbiAgICAgICAgY29uc3QgbGRhdGFfdXBkOiBLID0gdGhpcy5yZXNvbHZlTW9kZWwoIGRhdGFfdXBkICk7XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSggYXN5bmMgKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRiID0gYXdhaXQgdGhpcy5wRGIuY29ubmVjdCgpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgZGIuY29sbGVjdGlvbiggdGhpcy5jb2xsZWN0aW9uICkudXBkYXRlT25lKCBkYXRhLCB7ICRzZXQ6IGxkYXRhX3VwZCB9ICk7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKCByZXN1bHQgKTtcclxuICAgICAgICAgICAgfSBjYXRjaCggZXJyb3IgKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QoIGVycm9yICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZU1hbnkoIGRhdGE6IFUsIGRhdGFfdXBkOiBUfEsgKTogUHJvbWlzZTxVcGRhdGVXcml0ZU9wUmVzdWx0PiB7XHJcblxyXG4gICAgICAgIGNvbnN0IGxkYXRhX3VwZDogSyA9IHRoaXMucmVzb2x2ZU1vZGVsKCBkYXRhX3VwZCApO1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoIGFzeW5jIChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYiA9IGF3YWl0IHRoaXMucERiLmNvbm5lY3QoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IGRiLmNvbGxlY3Rpb24oIHRoaXMuY29sbGVjdGlvbiApLnVwZGF0ZU1hbnkoIGRhdGEsIHsgJHNldDogbGRhdGFfdXBkIH0gKTtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoIHJlc3VsdCApO1xyXG4gICAgICAgICAgICB9IGNhdGNoKCBlcnJvciApIHtcclxuICAgICAgICAgICAgICAgIHJlamVjdCggZXJyb3IgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgaW5zZXJ0T25lKCBkYXRhOiBUfEsgKTogUHJvbWlzZTxJbnNlcnRPbmVXcml0ZU9wUmVzdWx0PiB7XHJcblxyXG4gICAgICAgIGNvbnN0IGxkYXRhOiBLID0gdGhpcy5yZXNvbHZlTW9kZWwoIGRhdGEgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKCBhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGIgPSBhd2FpdCB0aGlzLnBEYi5jb25uZWN0KCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBkYi5jb2xsZWN0aW9uKCB0aGlzLmNvbGxlY3Rpb24gKS5pbnNlcnRPbmUoIGxkYXRhICk7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKCByZXN1bHQgKTtcclxuICAgICAgICAgICAgfSBjYXRjaCggZXJyb3IgKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QoIGVycm9yICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGluc2VydE1hbnkoIGRhdGFzOiAoVHxLKVtdICk6IFByb21pc2U8SW5zZXJ0V3JpdGVPcFJlc3VsdD4ge1xyXG5cclxuICAgICAgICBjb25zdCBsZGF0YXM6IEtbXSA9IHRoaXMucmVzb2x2ZU1vZGVscyggZGF0YXMgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKCBhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGIgPSBhd2FpdCB0aGlzLnBEYi5jb25uZWN0KCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBkYi5jb2xsZWN0aW9uKCB0aGlzLmNvbGxlY3Rpb24gKS5pbnNlcnRNYW55KCBsZGF0YXMgKTtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoIHJlc3VsdCApO1xyXG4gICAgICAgICAgICB9IGNhdGNoKCBlcnJvciApIHtcclxuICAgICAgICAgICAgICAgIHJlamVjdCggZXJyb3IgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImV4cHJlc3NcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibW9uZ29kYlwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ1dGlsXCIpOyJdLCJzb3VyY2VSb290IjoiIn0=